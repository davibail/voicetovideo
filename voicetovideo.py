import sys
import os
import time
import json
import argparse
import wave
import cv2
from vosk import Model, KaldiRecognizer
import subprocess


# Helper class for the word to phonemes conversion
class Word2Phonemes:
    def __init__ (self, configfile):
        with open(configfile, 'r', encoding='utf-8') as f:
            self.phonemes = json.loads(f.read())
        
    
    def getPhonemes(self, word):
        result = []
        length = len(word)
        start = 0
        while start < length:
            end = 3
            while end > 0:
                key = word[start:start+end]
                # print (key)
                if key in self.phonemes:
                    # print (self.phonemes[key])
                    for code in self.phonemes[key]:
                        result.append(code)
                    break
                end -= 1
            if end < 1:
                end = 1
            start = start + end
        return result



def main(argv):
    parser = argparse.ArgumentParser(description='Voice to video converted')
    parser.add_argument('-i', '--input', metavar='path/to/input.xxx', help='path to the input audio file', required=True)
    parser.add_argument('-o', '--output', metavar='path/to/output.mp4', help='path to the output video file', required=True)
    parser.add_argument('-d', '--dictionary', metavar='path/to/dictionary.json', help='path to dictionary json file (default is "dictionary.json")')
    parser.add_argument('-m', '--model', metavar='path/to/the/vosk/model/', help='path to the vosk model (download from https://alphacephei.com/vosk/models) (default is "model")')
    parser.add_argument('--debug', metavar='debug', help='enable debug')

    args = parser.parse_args()
    debug = False
    config={}
    config['dictionary'] = 'dictionary.json'
    config['model'] = 'model'
    if args.model:
        config['model'] = args.model
    if args.dictionary:
        config['dictionary'] = args.dictionary
    if args.input:
        config['input'] = args.input
    if args.output:
        config['output'] = args.output
    if args.debug:
        debug = True
    word2phonemes = Word2Phonemes(config['dictionary'])
    tmp=str(int(time.time()))


    # step 1
    print ('converting input file to wav format ...')
    cmd = 'ffmpeg -y -i '+config['input']+' -ar 16000 -ac 1 '+tmp+'.wav'
    subprocess.call(cmd, shell=True)
    print ('converting input file to wav format done')


    # step 2
    print ('loading converted audio and processing it ...')
    # loading the vosk model
    results = []
    model = Model(config['model'])

    f = wave.open(tmp+'.wav', 'rb')
    # intializing the speech to text engine with the timestamps activated
    rec = KaldiRecognizer(model, f.getframerate())
    rec.SetWords(True)

    # recognize speech using vosk model
    while True:
        data = f.readframes(4000)
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            part_result = json.loads(rec.Result())
            results.append(part_result)
    part_result = json.loads(rec.FinalResult())
    results.append(part_result)
    f.close()

    # extract the words structures into a proper array
    words = []
    for sentence in results:
        if len(sentence) == 1:
            # sometimes there are bugs in recognition 
            # and it returns an empty dictionary
            # {'text': ''}
            continue
        for obj in sentence['result']:
            words.append(obj)  # and add it to list

    if debug:
        with open('words.json', 'w') as f:
            json.dump(words, f, indent = 2)
    print ('loading converted audio and processing it done')


    # step 3 and 4
    print('creation of the video ...')
    width = 1920
    height = 1080
    framerate = 25.0
    fourcc = cv2.VideoWriter_fourcc(*'mp4v') # Be sure to use lower case
    out = cv2.VideoWriter(tmp+'.mp4', fourcc, framerate, (width, height))
    frame_index = 0 # to avoid introducing offset it is better to have an integer and to recompute the actual time every frame

    # loop on the words keeping track of the time
    for obj in words:
        word = obj['word'].lower()
        word = ''.join(ch for ch in word if ch.isalpha()) # removing non alpha chars
        word = '^'+word+'$' # adding regex chars
        start = obj['start']

        # this is the actuel step 3 that is done for each word
        phonemes = word2phonemes.getPhonemes(obj['word'].lower())

        # pad the video output with default frames up to the start of the word
        while frame_index/framerate < start:
            print('default ('+str(frame_index/framerate)+')')
            frame = cv2.imread('frames/default.png')
            out.write(frame)
            frame_index += 1

        if len(phonemes) > 0:
            end = obj['end']
            phoneme_duration=(end-start)/len(phonemes) # each phoneme will get the same screen time in average
            next_timestamp = start
            for phoneme in phonemes:
                next_timestamp += phoneme_duration # end of the current phoneme
                while frame_index/framerate < next_timestamp:
                    frame = cv2.imread('frames/'+phoneme+'.png')
                    out.write(frame)
                    frame_index += 1

    # close the video file
    out.release()
    print('creation of the video done')


    # step 5
    print('merging video and audio ...')
    cmd = 'ffmpeg -y -i '+tmp+'.mp4 -i '+tmp+'.wav -c:v copy -map 0:v:0 -map 1:a:0 -c:a aac -b:a 192k '+config['output']
    subprocess.call(cmd, shell=True)
    print('merging video and audio done')
    if debug:
        print(tmp+'.wav and '+tmp+'.mp4 are temporary files used for the creation of the final video')
    else:
        os.remove(tmp+'.wav')
        os.remove(tmp+'.mp4')



if __name__ == '__main__':
    main(sys.argv[1:])