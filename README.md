# Voice to video

## Description
Transform a voice recording into a video file using a speech to text recognition, a simple text to phoneme conversion and a set of phoneme linked images.\
The process is devided into steps:
1) Using ffmpeg to convert the intput audio file into wav file
2) Using VOSK, convert the wav file into an array of word structure with start and end timestamps
3) Using an homemade conversion system, transforms each word into an array of phonemes
4) Using the phonemes, the timestamps and the associated images, constructs a video, frame by frame, of an animated character (or whatever you want)
5) Merges the video and audio to create the final video

## Installation
### linux
#### debian based
`sudo apt update`\
`sudo apt install ffmpeg`\
`ffmpeg -version`\
`pip install argparse wave vosk opencv-python`
#### fedora
`sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm`\
`sudo dnf install https://download1.rpmfusion.org/non'free/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm`\
`sudo dnf update`\
`sudo dnf install ffmpeg`\
`ffmpeg -version`\
`pip install argparse wave vosk opencv-python`

### windows
Nothing really automated here.
You can follow tutorials like [this tutorial](https://www.geeksforgeeks.org/how-to-install-ffmpeg-on-windows/).\
Once you have ffmpeg installed you can run the following commands in a PowerShell console:\
`pip install argparse wave vosk opencv-python`


## Usage

python voicetovideo.py -i recording.mp3 -o video.mp4

python voicetovideo.py -i recording.wav -o video.mp4 -m mymodel -d mydictionary.json

## Support
If you cannot understand the code, not much help I can provide, I am afraid

## Authors and acknowledgment
David BAILLY

## License
This project is under the GNU GENERAL PUBLIC LICENSE.\
You will find a copy of the license in the LICENSE.md

## Project status
Developp in my free time. Don't expect top notch support
